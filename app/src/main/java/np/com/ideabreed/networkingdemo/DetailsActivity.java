package np.com.ideabreed.networkingdemo;

import android.content.Intent;
import android.os.Bundle;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import java.util.Objects;

import np.com.ideabreed.networkingdemo.database.DBPostModel;
import np.com.ideabreed.networkingdemo.database.DatabaseHandler;

public class DetailsActivity extends AppCompatActivity {

    TextView postId, userId, title, body;
    DatabaseHandler db;
    int id;

    @Override
    protected void onCreate(Bundle savedInstanceState){
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_details);

        postId = findViewById(R.id.id);
        userId = findViewById(R.id.userid);
        title = findViewById(R.id.title);
        body = findViewById(R.id.body);

        db = new DatabaseHandler(this);

        Intent intent = getIntent();
        id = Objects.requireNonNull(intent.getExtras()).getInt("id");

        Toast.makeText(this, "" + id, Toast.LENGTH_SHORT).show();
        //
        DBPostModel model = db.getOneData(id);

        postId.setText(String.valueOf(model.getId()));
        userId.setText(String.valueOf(model.getUserId()));
        title.setText(model.getTitle());
        body.setText(model.getBody());


    }
}
