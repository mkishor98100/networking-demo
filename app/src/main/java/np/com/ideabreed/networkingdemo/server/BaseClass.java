package np.com.ideabreed.networkingdemo.server;

import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

import static np.com.ideabreed.networkingdemo.utils.ConstantData.BASE_URL;

public class BaseClass {

    private static Retrofit instance = null;

    public static Retrofit getInstance(){

        if(instance == null) {

            instance = new Retrofit.Builder().baseUrl(BASE_URL)
                                             .addConverterFactory(GsonConverterFactory.create())
                                             .build();

        }


        return instance;

    }


}
