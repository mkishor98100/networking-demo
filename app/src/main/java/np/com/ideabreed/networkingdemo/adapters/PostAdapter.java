package np.com.ideabreed.networkingdemo.adapters;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.util.List;

import np.com.ideabreed.networkingdemo.DetailsActivity;
import np.com.ideabreed.networkingdemo.R;
import np.com.ideabreed.networkingdemo.database.DBPostModel;

public class PostAdapter extends RecyclerView.Adapter<PostAdapter.PostViewHolder> {

    private Context context;
    private List<DBPostModel> postModelList;

    public PostAdapter(Context context, List<DBPostModel> postModelList){
        this.context = context;
        this.postModelList = postModelList;
    }

    @NonNull
    @Override
    public PostViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType){

        View view = LayoutInflater.from(parent.getContext())
                                  .inflate(R.layout.post_item, parent, false);

        return new PostViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull PostViewHolder holder, int position){

        DBPostModel model = postModelList.get(position);

        holder.id.setText(String.valueOf(model.getId()));
        holder.title.setText(model.getTitle());
        holder.body.setText(model.getBody());


    }

    @Override
    public int getItemCount(){
        return postModelList.size();
    }

    public class PostViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        TextView id, title, body;

        public PostViewHolder(@NonNull View itemView){
            super(itemView);

            id = itemView.findViewById(R.id.post_id);
            title = itemView.findViewById(R.id.post_title);
            body = itemView.findViewById(R.id.post_body);


            itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View view){

            int position = getAdapterPosition();
            DBPostModel model = postModelList.get(position);

            Intent intent = new Intent(context, DetailsActivity.class);
            intent.putExtra("id", model.getId());
            context.startActivity(intent);


        }
    }


}
