package np.com.ideabreed.networkingdemo.utils;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;

public class ConstantData {

    public static final String BASE_URL = "https://jsonplaceholder.typicode.com/";

    public static final String DATABASE_NAME = "postDatabase";
    public static final int DATABASE_VERSION = 1;
    public static final String POST_TABLE = "posts";
    public static final String ID = "id";
    public static final String USERID = "userId";
    public static final String TITLE = "title";
    public static final String BODY = "body";


    public static boolean checkNetwork(Context context){

        ConnectivityManager manager =
                (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);

        assert manager != null;
        NetworkInfo info = manager.getActiveNetworkInfo();

        if(info != null && info.isConnected()) {
            return true;
        }
        return false;


    }

}
