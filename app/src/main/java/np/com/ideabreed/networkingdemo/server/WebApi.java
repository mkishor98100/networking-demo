package np.com.ideabreed.networkingdemo.server;

import java.util.List;

import np.com.ideabreed.networkingdemo.models.PostModel;
import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.Headers;
import retrofit2.http.POST;

public interface WebApi {

    @Headers("Cache-control: no-cache")
    @GET("posts")
    Call<List<PostModel>> getAllPost();

    @Headers("Cache-control: no-cache")
    @POST("posts")
    @FormUrlEncoded
    Call<PostModel> postToServer(@Field("title") String title, @Field("body") String body,
                                 @Field("userId") String userId);


}
