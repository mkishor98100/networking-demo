package np.com.ideabreed.networkingdemo.database;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

import static np.com.ideabreed.networkingdemo.utils.ConstantData.BODY;
import static np.com.ideabreed.networkingdemo.utils.ConstantData.DATABASE_NAME;
import static np.com.ideabreed.networkingdemo.utils.ConstantData.DATABASE_VERSION;
import static np.com.ideabreed.networkingdemo.utils.ConstantData.ID;
import static np.com.ideabreed.networkingdemo.utils.ConstantData.POST_TABLE;
import static np.com.ideabreed.networkingdemo.utils.ConstantData.TITLE;
import static np.com.ideabreed.networkingdemo.utils.ConstantData.USERID;

public class DatabaseHandler extends SQLiteOpenHelper {

    private Context context;

    public DatabaseHandler(Context context){
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
        this.context = context;
    }

    //insert all data
    public void insertPostIntoDb(DBPostModel postModel){

        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues contentValues = new ContentValues();

        contentValues.put(USERID, postModel.getUserId());
        contentValues.put(TITLE, postModel.getTitle());
        contentValues.put(BODY, postModel.getBody());

        long id = db.insert(POST_TABLE, null, contentValues);
        Toast.makeText(context, "" + id, Toast.LENGTH_SHORT).show();
        db.close();


    }

    //fetch all data
    public List<DBPostModel> getAllPosts(){

        List<DBPostModel> postModelList = new ArrayList<>();
        String sql = "SELECT * FROM " + POST_TABLE;
        SQLiteDatabase db = this.getWritableDatabase();

        Cursor cursor = db.rawQuery(sql, null);

        if(cursor.moveToFirst()) {
            do {

                DBPostModel model = new DBPostModel();
                model.setId(Integer.parseInt(cursor.getString(0)));
                model.setUserId(Integer.parseInt(cursor.getString(1)));
                model.setTitle(cursor.getString(2));
                model.setBody(cursor.getString(3));


                postModelList.add(model);


            } while(cursor.moveToNext());


        }

        cursor.close();
        return postModelList;


    }


    //fetch data by id
    public DBPostModel getOneData(int id){
        SQLiteDatabase db = this.getWritableDatabase();

        Cursor cursor = db.query(POST_TABLE, new String[]{ID, USERID, TITLE, BODY
        }, ID + "=?", new String[]{String.valueOf(id)}, null, null, null, null);

        if(cursor != null) { cursor.moveToFirst(); }

        DBPostModel test = new DBPostModel(Integer.parseInt(cursor.getString(0)),
                                           Integer.parseInt(cursor.getString(1)), cursor
                .getString(2), cursor.getString(3));

        cursor.close();

        return test;


    }

    //update data

    public int updateData(DBPostModel dbPostModel){

        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues contentValues = new ContentValues();
        contentValues.put(TITLE, dbPostModel.getTitle());
        contentValues.put(BODY, dbPostModel.getBody());


        int res = db.update(POST_TABLE, contentValues, ID + " =?", new String[]{
                String.valueOf(dbPostModel.getId())
        });
        db.close();
        return res;


    }


    //delete data

    public int deleteData(int id){
        SQLiteDatabase db = this.getWritableDatabase();

        int res = db.delete(POST_TABLE, ID + "=?", new String[]{String.valueOf(id)});


        db.close();
        return res;


    }

    @Override
    public void onCreate(SQLiteDatabase db){

        String SQL_QUERY =
                "CREATE TABLE " + POST_TABLE + "(" + ID + " INTEGER PRIMARY KEY ," + USERID + " " + "INTEGER," + TITLE + " TEXT," + BODY + " TEXT" + ")";
        db.execSQL(SQL_QUERY);


    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int i, int i1){

        db.execSQL("DROP TABLE IF EXISTS " + POST_TABLE);
        onCreate(db);

    }


}
