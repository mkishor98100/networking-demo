package np.com.ideabreed.networkingdemo;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

import np.com.ideabreed.networkingdemo.models.PostModel;
import np.com.ideabreed.networkingdemo.server.BaseClass;
import np.com.ideabreed.networkingdemo.server.WebApi;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class PostActivity extends AppCompatActivity implements View.OnClickListener {

    EditText titleInput, bodyInput, useridinput;
    Button submit;
    TextView responseFromServer;

    @Override
    protected void onCreate(Bundle savedInstanceState){
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_post);

        titleInput = findViewById(R.id.title);
        bodyInput = findViewById(R.id.body);
        useridinput = findViewById(R.id.userId);
        submit = findViewById(R.id.btnSubmit);
        responseFromServer = findViewById(R.id.response_from_server);


        submit.setOnClickListener(this);


    }

    @Override
    public void onClick(View view){

        String title = titleInput.getText().toString();
        String body = bodyInput.getText().toString();
        String userId = useridinput.getText().toString();

        final WebApi api = BaseClass.getInstance().create(WebApi.class);
        api.postToServer(title, body, userId).enqueue(new Callback<PostModel>() {
            @Override
            public void onResponse(Call<PostModel> call, Response<PostModel> response){


                if(response.isSuccessful()) {

                    PostModel model = response.body();
                    StringBuilder builder = new StringBuilder();
                    assert model != null;
                    builder.append("id:").append(String.valueOf(model.getId())).append("\n");
                    builder.append("Title:").append(model.getTitle()).append("\n");
                    builder.append("Body:").append(model.getBody()).append("\n");
                    builder.append("userId:")
                           .append(String.valueOf(model.getUserId()))
                           .append("\n");


                    responseFromServer.setText(builder.toString());

                }
            }

            @Override
            public void onFailure(Call<PostModel> call, Throwable t){
                responseFromServer.setText(t.getMessage());
            }
        });


    }
}
